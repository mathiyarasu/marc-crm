package com.marc.crm.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.marc.crm.dto.ErrorCodeDTO;
import com.marc.crm.dto.ResponseDto;
import java.util.function.Function;
import java.util.function.Predicate;

public class Assistant {

	public static boolean validateBody(String jsonReq) {
		boolean b = true;
		if (jsonReq != null) {
			jsonReq = jsonReq.trim().toUpperCase();
			if (jsonReq.contains("<SCRIPT") || jsonReq.contains("<OBJECT") || jsonReq.contains("<APPLET")
					|| jsonReq.contains("</SCRIPT>") || jsonReq.contains("<IMG ") || jsonReq.contains("<EMBED")
					|| jsonReq.contains("ALERT(") || jsonReq.contains("CONFIRM(") || jsonReq.contains("PROMPT(")
					|| jsonReq.contains("&GT;") || jsonReq.contains("&LT;") || jsonReq.contains("<META")
					|| jsonReq.contains("JAVASCRIPT")) {
				b = false;
			} else {
				b = true;
			}
		}
		return b;
	}

	public static boolean validateBody(String jsonReq, ResponseDto responseDto, Logger log) {
		boolean flag = validateBody(jsonReq);
		if (!flag) {
			ErrorCodeDTO error = new ErrorCodeDTO();
			List<ErrorCodeDTO> errorsList = new ArrayList<ErrorCodeDTO>();
			error.setErrorCode("ER-001");
			error.setErrorDesc("Request Validation Failure");
			errorsList.add(error);
			responseDto.setErrors(errorsList);
			responseDto.setStatus(false);
			log.debug("Request Validation Failure");
		}
		return flag;
	}

	public static ResponseDto addErrorResponse(String path, MarcException ex, ResponseDto responseDto, Logger log) {
		log.debug("Exception in path " + path + " : " + ex.getErrorMessage());
		ErrorCodeDTO error = new ErrorCodeDTO();
		List<ErrorCodeDTO> errorsList = new ArrayList<ErrorCodeDTO>();
		error.setErrorCode(ex.getErrorCode());
		error.setErrorDesc(ex.getErrorMessage());
		errorsList.add(error);
		responseDto.setErrors(errorsList);
		responseDto.setStatus(false);
		return responseDto;
	}

	public static <T> T convertJSONtoEntity(Map<?, ?> args, String entityName, Class<T> type)
			throws JsonMappingException, JsonProcessingException {
		T value = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
			if (args.get(entityName) != null)
				value = mapper.readValue(mapper.writeValueAsString(args.get(entityName)), type);
			else
				value = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	public static String ntechEncode(String args) {
		return new BCryptPasswordEncoder().encode(args);
	}

	public static boolean ntechDecode(String pwdText, String encryPass) {
		return new BCryptPasswordEncoder().matches(pwdText, encryPass);
	}

	public static String ntechPassgenerator(String args) {
		Random rand = new Random();
		return args.concat("" + rand.nextInt(1000));
	}

	public static <T> T nvl(T arg0, T arg1) {
		return (arg0 == null) ? arg1 : arg0;
	}

	public static String generateLeadNumber(String args) {

		if (args.length() >= GlobalConstants.leadlength) {
			return args;
		}
		StringBuilder sb = new StringBuilder();
		while (sb.length() < GlobalConstants.leadlength - args.length()) {
			sb.append('0');
		}
		sb.append(args);

		return "1" + sb.toString();
	}

	public static String generateConsumerNumber(String args) {
		if (args.length() >= GlobalConstants.conslength) {
			return args;
		}
		StringBuilder sb = new StringBuilder();
		while (sb.length() < GlobalConstants.conslength - args.length()) {
			sb.append('0');
		}
		sb.append(args);

		return "1" + sb.toString();
	}

	public static String generateRandomPassword(int len) {
		String chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi" + "jklmnopqrstuvwxyz!@#$%&";
		Random rnd = new Random();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(chars.charAt(rnd.nextInt(chars.length())));
		return sb.toString();
	}

	public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
		Map<Object, Boolean> map = new ConcurrentHashMap<>();
		return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
	}

	public static String ObjectIntoJson(Object obj) {
		ObjectMapper objMap = new ObjectMapper();
		String retValue = null;
		try {
			retValue = objMap.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
		}
		return retValue;
	}

	public static String getClobString(Clob clob) throws SQLException, IOException {
		BufferedReader stringReader = new BufferedReader(clob.getCharacterStream());
		String singleLine = null;
		StringBuffer strBuff = new StringBuffer();
		while ((singleLine = stringReader.readLine()) != null) {
			strBuff.append(singleLine);
		}
		return strBuff.toString();
	}
}
