package com.marc.crm.util;

public class MarcException extends RuntimeException {

	/**
	 * 
	 **/
	private static final long serialVersionUID = 1L;
	private String errorCode;
	private String errorMessage;

	public MarcException(String errorCode, String errorMessage) {
		super(errorMessage);
		this.errorCode = errorCode;
		this.errorMessage = findErrorMsg(errorCode, errorMessage);
	}

	public MarcException(String errorCode, String errorMessage, Exception ex) {
		super(errorMessage);
		this.errorCode = errorCode;
		this.errorMessage = findErrorMsg(errorCode, errorMessage);
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	// FIND ERROR MESSAGE

	public String findErrorMsg(String errorCode, String errorMessage) {
		if (errorMessage.contains("ConstraintViolationException"))
			return ErrorCodes.USER_DUBLICATE;
		else
			return ErrorCodes.UNKNOWN_ERROR;
	}
}
