package com.marc.crm.util;

public class ErrorCodes {

	public static String USER_DUBLICATE = "User email id already registerd.";
	public static String UNKNOWN_ERROR = "Unknow error please contact administrater.";
	

	/** The Constant EXCEPTION_AUTHENTICATION. */
	public static final String EXCEPTION_AUTHENTICATION = "Authentication Exception";
	
	/** The Constant ERROR_VALIDATION. */
	public static final String ERROR_VALIDATION = "Validation Error";
	
	/**The constant Label contact*/
	public static final String LBL_CONTACT = "contact";
	/**The constant http success*/
	public static final int HTTP_SUCCESS = 200;
	/**The constant for otp sms content*/
	public static final String OTP_SMS_CONTENT = "Hi your One Time Password for KKT registration is %s";
	/**The constant success status*/
	public static final String SUCCESS = "success";
	/**The constant for exception status*/
	public static final String EXCEPTION = "exception";
	/**The constant for failure status*/
	public static final String FAILURE = "failure";
	/**The constant for hashcode method*/
	public static final int PRIME = 31;
	
	/**The constant for image type*/
	public static final String IMAGE_FILE = "image";
	
	/**The constant for video type*/
	public static final String VIDEO_FILE = "video";
	
	/**The constant for jpg image format type*/
	public static final String IMAGE_JPG = "jpg";
	/**The constant for png image format type*/
	public static final String IMAGE_PNG = "png";
	/** The constant for Invalid UserName */
	public static final String INVALID_USERNAME = "Invalid UserName";
	
	// Role constants
	/**Constant for super admin*/
	public static final int ROLE_SUPERADMIN = 1;
	/**Constant for  admin*/
	public static final int ROLE_ADMIN = 2;
	/**Constant for memeber*/
	public static final int ROLE_MEMBER = 3;
	
	/** For Canada bot metrics */

	public static final String GET_BOT_CHAT_COUNT_URL = "/api/metrics/getChatCountInPeriods";
	public static final String GET_BOT_LOSS_CONNEC_URL = "/api/metrics/getCountOfConnectionLoss";
	public static final String GET_BOT_SUCC_CLOSURE_URL = "/api/metrics/getBotClosureMetrics";
	public static final String GET_BOT_COUNT_ENROLL_URL = "/api/metrics/getEnrollmentCount";
	public static final String GET_BOT_COUNT_SERVERDOWN_URL = "/api/metrics/getCountOfServerDown";

}
