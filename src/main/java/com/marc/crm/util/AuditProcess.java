package com.marc.crm.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.marc.crm.entity.AuditUser;
import com.marc.crm.repository.MarcAuditUserRepository;

@Component
public class AuditProcess {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private MarcAuditUserRepository auditUserRepository;

	public void createAuditRecord(String userid, String eventId, String eventDesc, String eventType, Object newData,
			Object oldData, String className, String p1, String p2, String p3) {
		AuditUser auditUser = null;
		try {
			auditUser = new AuditUser();
			auditUser.setCreatedBy(userid);
			auditUser.setEventDesc(eventDesc);
			auditUser.setEventId(eventId);
			auditUser.setEventType(eventType);
			// auditUser.setNewdata(newData);
			// auditUser.setOlddata(oldData);
			auditUser.setObjecttype(className);
			auditUser.setParam1(p1);
			auditUser.setParam2(p2);
			auditUser.setParam3(p3);
			auditUser.setStatus("A");
			auditUser.setUserid(userid);
			auditUserRepository.save(auditUser);
		} catch (Exception ex) {
			throw new MarcException("AUDIT-001", ex.getMessage());
		}
	}

	public void generateAuditRecord(String screenName, Object reqData, Object resData) {
		try {
			
			
			
		} catch (Exception ex) {
			throw new MarcException("AUDIT-002", ex.getMessage());
		}
	}
}