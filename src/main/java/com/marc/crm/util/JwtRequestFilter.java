package com.marc.crm.util;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.ExpiredJwtException;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

	Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private UserDetailsService jwtUserDetailsService;

	@Autowired
	private JWTUtility jwtTokenUtil;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {

		final String requestTokenHeader = request.getHeader("Authorization");
		String path = ((HttpServletRequest) request).getRequestURI();

//		// if (path.startsWith("/marc-crm/marc-admin/loginuser")) {
//		if (path.endsWith("/marc-admin/loginuser") || path.endsWith("/marc-admin/forgetpassword")
//				|| path.endsWith("/marc-admin/getkey") || path.endsWith("/swagger-ui.html")
//				|| path.endsWith("/api-docs") || path.equalsIgnoreCase("") || path.contains("csrf")
//				|| path.contains("swagger")) {
			if (true) {
			log.info("Generating token form credentials");
			chain.doFilter(request, response); // Just continue chain.
		} else {
			log.info("Validating token from header");
			String username = null;
			String jwtToken = null;
			// JWT Token is in the form "Bearer token". Remove Bearer word and get
			// only the Token
//			if (requestTokenHeader != null) {
//				jwtToken = requestTokenHeader;// .substring(7);
//				try {
//					username = jwtTokenUtil.getUsernameFromToken(jwtToken);
//				} catch (IllegalArgumentException e) {
//					log.debug("Unable to get JWT Token");
//					response.sendError(401,"Unable to get JWT Token");
//				} catch (ExpiredJwtException e) {
//					log.debug("JWT Token has expired");
//					throw e;
//				}
//			} else {
//				log.debug("JWT Token Missing");
//				response.sendError(401, "No Token found");
//				chain.doFilter(request, response);
//			}
//
//			// Once we get the token validate it.
//			if (username != null) {
//
//				UserDetails userDetails = this.jwtUserDetailsService.loadUserByUsername(username);
//
//				// if token is valid configure Spring Security to manually set
//				// authentication
//				if (jwtTokenUtil.validateToken(jwtToken, userDetails)) {
//
//					UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
//							userDetails, null, userDetails.getAuthorities());
//					usernamePasswordAuthenticationToken
//							.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
//					// After setting the Authentication in the context, we specify
//					// that the current user is authenticated. So it passes the
//					// Spring Security Configurations successfully.
//					SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
//					log.debug("Token Validated");
//				}
				chain.doFilter(request, response);
			}
		}

	//}

}
