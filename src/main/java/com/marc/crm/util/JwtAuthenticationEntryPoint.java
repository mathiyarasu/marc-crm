package com.marc.crm.util;

import java.io.IOException;
import java.io.Serializable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {
	Logger log = LoggerFactory.getLogger(this.getClass());
	private static final long serialVersionUID = -7858869558953243875L;

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException {
		log.info("Thread Excep : " + Thread.currentThread().getId());
		response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
				"Unauthorized :" + authException.getMessage() + ": thread :" + Thread.currentThread().getId());
	}

}
