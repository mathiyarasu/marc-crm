package com.marc.crm.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.marc.crm.service.MarcUserDetailsService;
import com.marc.crm.util.JwtAuthenticationEntryPoint;
import com.marc.crm.util.JwtRequestFilter;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	private static final String ADMIN = "ADMIN";
	private static final String USER = "USER";
	private static final String PUBLIC = "PUBLIC";

	@Autowired
	private MarcUserDetailsService userDetailsService;

	@Autowired
	private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

	@Autowired
	private JwtRequestFilter jwtRequestFilter;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService);
	}

	private static final String[] AUTH_WHITELIST = { "/v2/api-docs", "/swagger-resources", "/swagger-resources/**",
			"/configuration/ui", "/configuration/security", "/swagger-ui.html", "/webjars/**" };

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().and().csrf().disable().authorizeRequests().antMatchers("/admin").hasRole(ADMIN).antMatchers("/user")
				.hasAnyRole(ADMIN, USER).antMatchers("/public").hasAnyRole(ADMIN, PUBLIC).antMatchers("/login")
				.permitAll().and().exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and()
				.formLogin();

		http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);

		/**
		 * // Enable CORS and disable CSRF http = http.cors().and().csrf().disable();
		 * 
		 * // Set session management to stateless http =
		 * http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and();
		 * 
		 * // Set unauthorized requests exception handler http =
		 * http.exceptionHandling().authenticationEntryPoint((request, response, ex) ->
		 * { response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
		 * }).and();
		 * 
		 * // Set permissions on endpoints http.authorizeRequests() // Our public
		 * endpoints .antMatchers("/**").permitAll() // Our private endpoints
		 * .anyRequest().authenticated();
		 * 
		 * // Add JWT token filter http.addFilterBefore(jwtRequestFilter,
		 * UsernamePasswordAuthenticationFilter.class);
		 **/
	}

	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManager();
	}

	@Bean
	public BCryptPasswordEncoder getPasswordEncoder() {
		return new BCryptPasswordEncoder(10);
	}

}
