package com.marc.crm.dto;

import java.util.Objects;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * ErrorCodeDTO
 */
@Validated

public class ErrorCodeDTO {
	@JsonProperty("errorCode")
	private String errorCode = null;

	@JsonProperty("errorDesc")
	private String errorDesc = null;

	public ErrorCodeDTO errorCode(String errorCode) {
		this.errorCode = errorCode;
		return this;
	}

	/**
	 * Get errorCode
	 * 
	 * @return errorCode
	 **/

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public ErrorCodeDTO errorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
		return this;
	}

	/**
	 * Get errorDesc
	 * 
	 * @return errorDesc
	 **/

	public String getErrorDesc() {
		return errorDesc;
	}

	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ErrorCodeDTO errorCodeDTO = (ErrorCodeDTO) o;
		return Objects.equals(this.errorCode, errorCodeDTO.errorCode)
				&& Objects.equals(this.errorDesc, errorCodeDTO.errorDesc);
	}

	@Override
	public int hashCode() {
		return Objects.hash(errorCode, errorDesc);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ErrorCodeDTO {\n");

		sb.append("    errorCode: ").append(toIndentedString(errorCode)).append("\n");
		sb.append("    errorDesc: ").append(toIndentedString(errorDesc)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
