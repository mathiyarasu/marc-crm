package com.marc.crm.dto;

import java.util.List;

public class ResponseDto {

	private boolean status;
	private List<Object> warning;
	private List<ErrorCodeDTO> Errors;
	private Object data;

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public List<Object> getWarning() {
		return warning;
	}

	public void setWarning(List<Object> warning) {
		this.warning = warning;
	}

	public List<ErrorCodeDTO> getErrors() {
		return Errors;
	}

	public void setErrors(List<ErrorCodeDTO> errors) {
		Errors = errors;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
