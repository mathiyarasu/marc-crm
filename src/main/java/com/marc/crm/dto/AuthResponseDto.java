package com.marc.crm.dto;

public class AuthResponseDto {

	private String username;
	private String marcTokan;
	private String role;
	private String actsno;

	public String getMarcTokan() {
		return marcTokan;
	}

	public void setMarcTokan(String marcTokan) {
		this.marcTokan = marcTokan;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getActsno() {
		return actsno;
	}

	public void setActsno(String actsno) {
		this.actsno = actsno;
	}

}
