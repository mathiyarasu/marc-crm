package com.marc.crm.dto;

import java.sql.Timestamp;

public class UserActivityDto {
	Timestamp loginat;
	Timestamp logoutat;

	public Timestamp getLoginat() {
		return loginat;
	}

	public void setLoginat(Timestamp loginat) {
		this.loginat = loginat;
	}

	public Timestamp getLogoutat() {
		return logoutat;
	}

	public void setLogoutat(Timestamp logoutat) {
		this.logoutat = logoutat;
	}
}
