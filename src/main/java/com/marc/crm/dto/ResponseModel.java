package com.marc.crm.dto;

import java.io.Serializable;

/**
 * The Class ResponseModel.
 */
public class ResponseModel implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The status. */
	private String status = "";

	/** The message. */
	private String message = "";

	/** The payload. */
	private Object payload;

	/**
	 * Instantiates a new response model.
	 */
	public ResponseModel() {
	}

	/**
	 * Instantiates a new response model.
	 *
	 * @param status  the status
	 * @param message the message
	 * @param payload the payload
	 */
	public ResponseModel(final String status, final String message, final Object payload) {
		this.status = status;
		this.message = message;
		this.payload = payload;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(final String status) {
		this.status = status;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(final String message) {
		this.message = message;
	}

	/**
	 * Gets the payload.
	 *
	 * @return the payload
	 */
	public Object getPayload() {
		return payload;
	}

	/**
	 * Sets the payload.
	 *
	 * @param payload the new payload
	 */
	public void setPayload(final Object payload) {
		this.payload = payload;
	}

}
