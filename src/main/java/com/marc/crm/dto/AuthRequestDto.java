package com.marc.crm.dto;

import java.io.Serializable;
import java.util.Map;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Item Details", description = "Item Details for perticular Item")
public class AuthRequestDto implements Serializable {

	private static final long serialVersionUID = -7844251211618763233L;

	@ApiModelProperty(name = "Username", value = "username", dataType = "String", required = true, example = "xxx@mail.com")
	private String username;

	@ApiModelProperty(name = "Password", value = "password", dataType = "String", required = true, example = "xxx")
	private String password;

	private Map<?, ?> actInfo;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Map<?, ?> getActInfo() {
		return actInfo;
	}

	public void setActInfo(Map<?, ?> actInfo) {
		this.actInfo = actInfo;
	}

	@Override
	public String toString() {
		return "AuthRequestDto [username=" + username + ", password=" + password + ", actInfo=" + actInfo + "]";
	}

}
