package com.marc.crm.process;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.marc.crm.entity.UserEntity;
import com.marc.crm.repository.MarcUserRepository;

@Component
public class MarcUserProcess {

	@Autowired
	MarcUserRepository marcUserRepository;

	public List<UserEntity> getUser() {
		List<UserEntity> userEntityList = marcUserRepository.findAll();
		return userEntityList;
	}

	public UserEntity addUser(UserEntity userEntity) {
		try {
			if (userEntity != null && !userEntity.getUserEmpName().equals("") && !userEntity.getUserDoj().equals("")
					&& !userEntity.getUserDesignation().equals("") && !userEntity.getUserAddress().equals("")
					&& !userEntity.getUserLandmark().equals("") && !userEntity.getUserCity().equals("")
					&& !userEntity.getUserState().equals("") && !userEntity.getUserCountry().equals("")
					&& !userEntity.getUserPincode().equals("") && !userEntity.getUserEmail().equals("")
					&& !userEntity.getUserActiveStatus().equals("")) {
				userEntity = marcUserRepository.save(userEntity);
				return userEntity;
			}
		} catch (Exception ex) {
			ex.printStackTrace();

		}
		return null;

	}

	public String deleteUser(Integer userId) {
		try {
			marcUserRepository.deleteById(userId);
			return "usereEntity is deleted";
		} catch (Exception ex) {
			return "userEntity is not deleted";

		}

	}

	public UserEntity updateUser(UserEntity userEntity) {
		marcUserRepository.save(userEntity);
		return userEntity;

	}

}
