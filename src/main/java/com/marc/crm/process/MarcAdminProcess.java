package com.marc.crm.process;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.marc.crm.dto.ResponseDto;
import com.marc.crm.entity.Geography;
import com.marc.crm.entity.UserDetails;
import com.marc.crm.repository.MarcAuthUserRepository;
import com.marc.crm.repository.MarcGeographyRepository;
import com.marc.crm.repository.MarcUserDetailsRepository;

@Component
public class MarcAdminProcess {

	Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	MarcUserDetailsRepository userRepository;
	@Autowired
	MarcGeographyRepository geographyRepository;
	@Autowired
	MarcAuthUserRepository authUserRepository;
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;

	ResponseDto responseDto = null;

	public boolean updateFirstlogin(String userid, String mode) {
		boolean status = false;
		try {
			UserDetails userdet = userRepository.findByUserid(userid);
			if (mode.equalsIgnoreCase("Y"))
				userdet.setFirstLogin("Y");
			else
				userdet.setFirstLogin("N");

			userRepository.save(userdet);
			status = true;
		} catch (Exception ex) {
			log.error("User updateFirstlogin Failed due to " + ex.getMessage());
		}
		return status;

	}

	public Set<Geography> getGeography(String id, String type) {
		Set<Geography> geographyList = new HashSet<>();

		try {

			if (null != id) {
				Optional<Geography> parent = geographyRepository.findById(Long.parseLong(id));

				if (parent.isPresent())
					geographyList = parent.get().getChildren();
			} else if (null != type) {
				geographyList = geographyRepository.findByType(type);
			}

		} catch (Exception ex) {
			log.info("Error in getGeography : " + ex.getMessage());
		}
		return geographyList;
	}

	public long bulkInsertGeography(List<Geography> geographyList) {

		int modified = 0;
		try {

			if (geographyList != null && !geographyList.isEmpty())
				for (Geography g : geographyList)
					modified += geographyRepository.insert(g.getCode(), g.getDescription(), g.getType(),
							g.getParentId());

		} catch (Exception ex) {
			log.info("Error in bulkInsertGeography : " + ex.getMessage());
		}
		return modified;
	}

	public Geography saveGeography(Geography geo) {
		try {

			if (geo != null) {
				String parentId = geo.getParentId();
				if (null != parentId) {
					Optional<Geography> parent = geographyRepository.findById(Long.parseLong(parentId));
					if (parent.isPresent()) {
						parent.get().getChildren().add(geo);
						geo.setParent(parent.get());
						geographyRepository.save(geo);
						geographyRepository.save(parent.get());
					}
				} else {
					geo = geographyRepository.save(geo);
				}

			}

		} catch (Exception ex) {
			log.info("Error in saveGeography : " + ex.getMessage());
		}
		return geo;
	}

}
