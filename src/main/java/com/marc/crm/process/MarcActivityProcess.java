package com.marc.crm.process;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.marc.crm.dto.UserActivityDto;
import com.marc.crm.entity.LoginDetails;
import com.marc.crm.repository.MarcLoginDetailsRepository;

@Component
public class MarcActivityProcess {

	Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private MarcLoginDetailsRepository detailsRepository;

	public Integer createActivityInfo(LoginDetails loginDet) {
		Integer sno = 0;
		try {

			sno = detailsRepository.save(loginDet).getSno();

		} catch (Exception ex) {
			log.debug("Exception on createActivityInfo " + ex.getMessage());
		}
		return sno;
	}

	public List<UserActivityDto> getActivityInfo(String userId) {
		List<UserActivityDto> returnVal = new ArrayList<>();
		UserActivityDto usrActivity = new UserActivityDto();
		try {
			List<Object[]> activity = detailsRepository.findActivity(userId);

			if (activity != null) {
				for (Object[] arg : activity) {
					usrActivity = new UserActivityDto();
					usrActivity.setLoginat((Timestamp) arg[0]);
					usrActivity.setLogoutat((Timestamp) arg[1]);
					returnVal.add(usrActivity);
				}
			}
			log.info("getActivityInfo : " + userId);
		} catch (Exception ex) {
			log.debug("Exception on getActivityInfo : " + userId);
		}
		return returnVal;
	}

	public boolean updateActivityInfo(String logoutMode, String key) {
		boolean status = false;
		try {
			LoginDetails loginDet = detailsRepository.findById(Integer.parseInt(key)).get();
			loginDet.setLogoutMode(logoutMode);
			detailsRepository.save(loginDet);
			status = true;

		} catch (Exception ex) {
			log.debug("Exception on updateActivityInfo " + ex.getMessage());
		}
		return status;
	}

	public String getActivitySno(String key) {
		String id = null;
		try {
			// id = detailsRepository.findSno(key);
		} catch (Exception ex) {
			log.debug("Exception on getActivitySno " + ex.getMessage());
		}
		return id;
	}
}
