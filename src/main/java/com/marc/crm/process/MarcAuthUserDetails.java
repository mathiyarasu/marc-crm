package com.marc.crm.process;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.marc.crm.entity.AuthUser;

public class MarcAuthUserDetails implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	AuthUser authUser;

	public MarcAuthUserDetails(AuthUser authUser) {
		this.authUser = authUser;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public String getPassword() {
		return authUser.getPwd();
	}

	@Override
	public String getUsername() {
		return authUser.getUserid();
	}

	@Override
	public boolean isAccountNonExpired() {
		if (authUser.getStatus().equalsIgnoreCase("A"))
			return true;
		else
			return false;
	}

	@Override
	public boolean isAccountNonLocked() {
		if (authUser.getStatus().equalsIgnoreCase("A"))
			return true;
		else
			return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {

		if (authUser.getPwdExpireDate() != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			Date curdate = new Date();
			Date expdate = authUser.getPwdExpireDate();
			formatter.format(curdate);
			formatter.format(expdate);

			if (expdate.compareTo(curdate) > 0)
				return true;
			else
				return false;
		} else
			return true;
	}

	@Override
	public boolean isEnabled() {
		if (authUser.getStatus().equalsIgnoreCase("A"))
			return true;
		else
			return false;

	}

}
