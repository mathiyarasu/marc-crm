package com.marc.crm.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "geography")
public class Geography {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long sno;
	
	private String code;
	
	private String type;
	
	private String description;
	
	@Transient
	private String parentId;
	
	@ManyToOne
	@JoinColumn(name="parent")
	private Geography parent;
		
	@OneToMany(mappedBy="parent",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	private Set<Geography> children = new HashSet<>();


	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	

	public long getSno() {
		return sno;
	}

	public void setSno(long sno) {
		this.sno = sno;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@JsonIgnore
	public Geography getParent() {
		return parent;
	}

	public void setParent(Geography parent) {
		this.parent = parent;
	}

	public Set<Geography> getChildren() {
		return children;
	}

	public void setChildren(Set<Geography> children) {
		this.children = children;
	}

	
	
	
}
