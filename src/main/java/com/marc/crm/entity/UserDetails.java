package com.marc.crm.entity;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.sql.Timestamp;
import java.math.BigInteger;

/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@Table(name = "user_details")
@NamedQuery(name = "UserDetails.findAll", query = "SELECT b FROM UserDetails b")

// (b.fname like coalesce(:name,b.fname) or b.lname like coalesce(:name,b.lname) or
public class UserDetails implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	@Column(name = "sno")
	private Integer sno;

	@Column(name = "userid")
	private String userid;
	@Column(name = "user_status")
	private String userStatus;
	@Column(name = "first_login")
	private String firstLogin;
	public Integer getSno() {
		return sno;
	}
	public void setSno(Integer sno) {
		this.sno = sno;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUserStatus() {
		return userStatus;
	}
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
	public String getFirstLogin() {
		return firstLogin;
	}
	public void setFirstLogin(String firstLogin) {
		this.firstLogin = firstLogin;
	}

}