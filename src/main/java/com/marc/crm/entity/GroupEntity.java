package com.marc.crm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "GROUP_DETAIL")
public class GroupEntity implements Serializable {
	private static final long seialiVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GROUP_DETAILP_SEQ")
	@SequenceGenerator(name = "GROUP_DETAIL_SEQ", sequenceName = "GROUP_DETAIL_SEQ", allocationSize = 1)
	@Column(name = "GROUP_ID")
	private int groupId;

	@Column(name = "GROUP_NAME")
	private String groupName;
	@Column(name = "GROUP_CONTACT_PERSON")
	private String groupContactPerson;
	@Column(name = "GROUP_EMAIL")
	private String groupEmail;
	@Column(name = "GROUP_PHONE")
	private String groupPhone;
	@Column(name = "GROUP_ACTIVE_STATUS")
	private String groupActiveStatus;

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupContactPerson() {
		return groupContactPerson;
	}

	public void setGroupContactPerson(String groupContactPerson) {
		this.groupContactPerson = groupContactPerson;
	}

	public String getGroupEmail() {
		return groupEmail;
	}

	public void setGroupEmail(String groupEmail) {
		this.groupEmail = groupEmail;
	}

	public String getGroupPhone() {
		return groupPhone;
	}

	public void setGroupPhone(String groupPhone) {
		this.groupPhone = groupPhone;
	}

	public String getGroupActiveStatus() {
		return groupActiveStatus;
	}

	public void setGroupActiveStatus(String groupActiveStatus) {
		this.groupActiveStatus = groupActiveStatus;
	}

	public static long getSeialiversionuid() {
		return seialiVersionUID;
	}

	public GroupEntity(int groupId, int groupCompId, int groupCompLocId, String groupName, String groupContactPerson,
			String groupEmail, String groupPhone, String groupActiveStatus) {
		super();
		this.groupId = groupId;

		this.groupName = groupName;
		this.groupContactPerson = groupContactPerson;
		this.groupEmail = groupEmail;
		this.groupPhone = groupPhone;
		this.groupActiveStatus = groupActiveStatus;
	}

	public GroupEntity() {

	}

}
