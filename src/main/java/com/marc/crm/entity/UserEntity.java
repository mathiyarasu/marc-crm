package com.marc.crm.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "USER")
public class UserEntity implements Serializable {
	private static final long seialiVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_SEQ")
	@SequenceGenerator(name = "USER_SEQ", sequenceName = "USER_SEQ", allocationSize = 1)
	@Column(name = "USER_ID")
	private int userId;
	@Column(name = "USER_EMP_ID")
	private int userEmpId;
	@Column(name = "USER_EMP_NAME")
	private String userEmpName;
	@Column(name = "USER_DOJ")
	private Date userDoj;
	@Column(name = "USER_DEGIGNATION")
	private String userDesignation;
	@Column(name = "USER_ADDRESS")
	private String userAddress;
	@Column(name = "USER_LANDMARK")
	private String userLandmark;
	@Column(name = "USER_CITY")
	private String userCity;
	@Column(name = "USER_STATE")
	private String userState;
	@Column(name = "USER_COUNTRY")
	private String userCountry;
	@Column(name = "USER_PINCODE")
	private String userPincode;
	@Column(name = "USER_EMAIL")
	private String userEmail;
	@Column(name = "USER_PHONE")
	private String userPhone;
	@Column(name = "USER_ACTIVE_STATUS")
	private String userActiveStatus;

	@ManyToOne
	@JoinColumn(name = "USER_GROUP_ID")
	private GroupEntity groupEntity;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getUserEmpId() {
		return userEmpId;
	}

	public void setUserEmpId(int userEmpId) {
		this.userEmpId = userEmpId;
	}

	public String getUserEmpName() {
		return userEmpName;
	}

	public void setUserEmpName(String userEmpName) {
		this.userEmpName = userEmpName;
	}

	public Date getUserDoj() {
		return userDoj;
	}

	public void setUserDoj(Date userDoj) {
		this.userDoj = userDoj;
	}

	public String getUserDesignation() {
		return userDesignation;
	}

	public void setUserDesignation(String userDesignation) {
		this.userDesignation = userDesignation;
	}

	public String getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}

	public String getUserLandmark() {
		return userLandmark;
	}

	public void setUserLandmark(String userLandmark) {
		this.userLandmark = userLandmark;
	}

	public String getUserCity() {
		return userCity;
	}

	public void setUserCity(String userCity) {
		this.userCity = userCity;
	}

	public String getUserState() {
		return userState;
	}

	public void setUserState(String userState) {
		this.userState = userState;
	}

	public String getUserCountry() {
		return userCountry;
	}

	public void setUserCountry(String userCountry) {
		this.userCountry = userCountry;
	}

	public String getUserPincode() {
		return userPincode;
	}

	public void setUserPincode(String userPincode) {
		this.userPincode = userPincode;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getUserActiveStatus() {
		return userActiveStatus;
	}

	public void setUserActiveStatus(String userActiveStatus) {
		this.userActiveStatus = userActiveStatus;
	}

	public GroupEntity getGroupEntity() {
		return groupEntity;
	}

	public void setGroupEntity(GroupEntity groupEntity) {
		this.groupEntity = groupEntity;
	}

	public static long getSeialiversionuid() {
		return seialiVersionUID;
	}

	public UserEntity(int userId, int userEmpId, String userEmpName, Date userDoj, String userDesignation,
			String userAddress, String userLandmark, String userCity, String userState, String userCountry,
			String userPincode, String userEmail, String userPhone, String userActiveStatus) {
		super();
		this.userId = userId;
		this.userEmpId = userEmpId;
		this.userEmpName = userEmpName;
		this.userDoj = userDoj;
		this.userDesignation = userDesignation;
		this.userAddress = userAddress;
		this.userLandmark = userLandmark;
		this.userCity = userCity;
		this.userState = userState;
		this.userCountry = userCountry;
		this.userPincode = userPincode;
		this.userEmail = userEmail;
		this.userPhone = userPhone;
		this.userActiveStatus = userActiveStatus;
	}

	public UserEntity() {

	}

}
