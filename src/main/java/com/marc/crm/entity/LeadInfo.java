package com.marc.crm.entity;

import java.io.Serializable;
import java.util.Objects;

public class LeadInfo implements Serializable{
	
	private Integer sno;
	private String leadNo;
	public Integer getSno() {
		return sno;
	}
	public void setSno(Integer sno) {
		this.sno = sno;
	}
	public String getLeadNo() {
		return leadNo;
	}
	public void setLeadNo(String leadNo) {
		this.leadNo = leadNo;
	}
	@Override
	public int hashCode() {
		return Objects.hash(leadNo, sno);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LeadInfo other = (LeadInfo) obj;
		return Objects.equals(leadNo, other.leadNo) && Objects.equals(sno, other.sno);
	}
	@Override
	public String toString() {
		return "LeadInfo [sno=" + sno + ", leadNo=" + leadNo + "]";
	}
	
	
	

}
