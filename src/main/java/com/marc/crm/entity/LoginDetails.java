package com.marc.crm.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "login_details")
@NamedQuery(name = " LoginDetails.findAll", query = "SELECT b FROM  LoginDetails b")
//@NamedQuery(name = " LoginDetails.findSno", query = "SELECT b.Sno FROM  LoginDetails b Where b.Sessionid = :sessionId ")

public class LoginDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "sno")
	private Integer sno;

	@Column(name = "userid")
	private String userid;

	@Column(name = "ipaddr")
	private String ipaddr;

	@Column(name = "sessionid")
	private String sessionid;

	@Column(name = "device_type")
	private String deviceType;

	@Column(name = "device_info")
	private String deviceInfo;

	@Column(name = "device_id")
	private String deviceId;

	@Column(name = "device_macaddr")
	private String deviceMacaddr;

	@Column(name = "active_time")
	private String activeTime;

	@Column(name = "status")
	private String status;

	@Column(name = "additionalinfo")
	private String additionalinfo;

	@CreationTimestamp
	@Column(name = "loginat")
	private Timestamp loginat;

	@UpdateTimestamp
	@Column(name = "logoutat")
	private Timestamp logoutat;

	@Column(name = "logout_mode")
	private String logoutMode;

	@Column(name = "login_mode")
	private String loginMode;

	public Integer getSno() {
		return sno;
	}

	public void setSno(Integer sno) {
		this.sno = sno;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getIpaddr() {
		return ipaddr;
	}

	public void setIpaddr(String ipaddr) {
		this.ipaddr = ipaddr;
	}

	public String getSessionid() {
		return sessionid;
	}

	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceMacaddr() {
		return deviceMacaddr;
	}

	public void setDeviceMacaddr(String deviceMacaddr) {
		this.deviceMacaddr = deviceMacaddr;
	}

	public String getActiveTime() {
		return activeTime;
	}

	public void setActiveTime(String activeTime) {
		this.activeTime = activeTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAdditionalinfo() {
		return additionalinfo;
	}

	public void setAdditionalinfo(String additionalinfo) {
		this.additionalinfo = additionalinfo;
	}

	public Timestamp getLoginat() {
		return loginat;
	}

	public void setLoginat(Timestamp loginat) {
		this.loginat = loginat;
	}

	public Timestamp getLogoutat() {
		return logoutat;
	}

	public void setLogoutat(Timestamp logoutat) {
		this.logoutat = logoutat;
	}

	public String getLogoutMode() {
		return logoutMode;
	}

	public void setLogoutMode(String logoutMode) {
		this.logoutMode = logoutMode;
	}

	public String getLoginMode() {
		return loginMode;
	}

	public void setLoginMode(String loginMode) {
		this.loginMode = loginMode;
	}

}
