package com.marc.crm.entity;

import java.io.Serializable;
import java.util.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "auth_user")
@NamedQuery(name = "AuthUser.findAll", query = "SELECT b FROM AuthUser b")
public class AuthUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	@Column(name = "sno")
	private Integer sno;
	
	@Column(name = "userid ")
	private String userid;

	@Column(name = "pwd")
	private String pwd;

	@Column(name = "pwd_expire_date")
	private Date pwdExpireDate;

	@Column(name = "status")
	private String status;

	@Column(name = "retry_count")
	private Integer retryCount;

	@Column(name = "first_login")
	private String firstLogin;

	@Column(name = "role")
	private String role;

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public Date getPwdExpireDate() {
		return pwdExpireDate;
	}

	public void setPwdExpireDate(Date date) {
		this.pwdExpireDate = date;
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(Integer retryCount) {
		this.retryCount = retryCount;
	}

	public String getFirstLogin() {
		return firstLogin;
	}

	public void setFirstLogin(String firstLogin) {
		this.firstLogin = firstLogin;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
