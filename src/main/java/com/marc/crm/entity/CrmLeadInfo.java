package com.marc.crm.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@IdClass(LeadInfo.class)
@Table(name = "crm_lead_info")
public class CrmLeadInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "sno", length = 8)
	private Integer sno;

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "lead_no")
    @GenericGenerator(
        name = "lead_no", 
        strategy = "com.marc.crm.entity.StringPrefixedSequenceIdGenerator", 
        parameters = {
            @Parameter(name = StringPrefixedSequenceIdGenerator.INCREMENT_PARAM, value = "50"),
            @Parameter(name = StringPrefixedSequenceIdGenerator.VALUE_PREFIX_PARAMETER, value = "CRM"),
            @Parameter(name = StringPrefixedSequenceIdGenerator.NUMBER_FORMAT_PARAMETER, value = "%05d") })
	@Column(name = "lead_no", length = 8)
	private String leadNo;
	
	@Column(name = "name", length = 100)
	private String name;

	@Column(name = "email", length = 100)
	private String email;

	@Column(name = "mobile", length = 15)
	private String mobile;

	@Column(name = "busi_desc", length = 500)
	private String busiDesc;

	@Column(name = "busi_ind", length = 2)
	private String busiInd;

	@Column(name = "busi_clas", length = 2)
	private String busiclas;

	@Column(name = "mclub", length = 2)
	private String mclub;

	@Column(name = "dir_id", length = 2)
	private String dirId;

	@Column(name = "state", length = 3)
	private Integer state;

	@Column(name = "city", length = 100)
	private Integer city;

	@Column(name = "adr1", length = 500)
	private String adr1;

	@Column(name = "adr2", length = 500)
	private String adr2;

	@Column(name = "cur_wf_id", length = 2)
	private String curWfId;

	@Column(name = "held_usr_id", length = 100)
	private String heldUsrId;

	@Column(name = "postcode", length = 6)
	private String postcode;

	@Column(name = "status")
	private String status;

	@Column(name = "incomeing_mode")
	private String incomeingMode;

	// AUDIT FIELDS

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "updated_by")
	private String updatedBy;

	@CreationTimestamp
	@Column(name = "created_date")
	private Timestamp createdDate;

	@UpdateTimestamp
	@Column(name = "updated_date")
	private Timestamp updatedDate;

	public Integer getSno() {
		return sno;
	}

	public void setSno(Integer sno) {
		this.sno = sno;
	}

	public String getLeadNo() {
		return leadNo;
	}

	public void setLeadNo(String leadNo) {
		this.leadNo = leadNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getBusiDesc() {
		return busiDesc;
	}

	public void setBusiDesc(String busiDesc) {
		this.busiDesc = busiDesc;
	}

	public String getBusiInd() {
		return busiInd;
	}

	public void setBusiInd(String busiInd) {
		this.busiInd = busiInd;
	}

	public String getBusiclas() {
		return busiclas;
	}

	public void setBusiclas(String busiclas) {
		this.busiclas = busiclas;
	}

	public String getMclub() {
		return mclub;
	}

	public void setMclub(String mclub) {
		this.mclub = mclub;
	}

	public String getDirId() {
		return dirId;
	}

	public void setDirId(String dirId) {
		this.dirId = dirId;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getCity() {
		return city;
	}

	public void setCity(Integer city) {
		this.city = city;
	}

	public String getAdr1() {
		return adr1;
	}

	public void setAdr1(String adr1) {
		this.adr1 = adr1;
	}

	public String getAdr2() {
		return adr2;
	}

	public void setAdr2(String adr2) {
		this.adr2 = adr2;
	}

	public String getCurWfId() {
		return curWfId;
	}

	public void setCurWfId(String curWfId) {
		this.curWfId = curWfId;
	}

	public String getHeldUsrId() {
		return heldUsrId;
	}

	public void setHeldUsrId(String heldUsrId) {
		this.heldUsrId = heldUsrId;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIncomeingMode() {
		return incomeingMode;
	}

	public void setIncomeingMode(String incomeingMode) {
		this.incomeingMode = incomeingMode;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Timestamp getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	

}