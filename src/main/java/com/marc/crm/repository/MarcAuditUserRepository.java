package com.marc.crm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.marc.crm.entity.AuditUser;

@Repository
public interface MarcAuditUserRepository extends JpaRepository<AuditUser, String> {

}
