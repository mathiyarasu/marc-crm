package com.marc.crm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.marc.crm.entity.LoginDetails;

@Repository
public interface MarcLoginDetailsRepository extends JpaRepository<LoginDetails, Integer> {

	@Query(value = "select loginat,logoutat from login_details where userid =?1 order by sno desc limit 3", nativeQuery = true)
	public List<Object[]> findActivity(String userid);

	// public String findSno(@Param("sessionId") String sessionid);
}
