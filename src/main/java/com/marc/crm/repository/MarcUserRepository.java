package com.marc.crm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.marc.crm.entity.UserEntity;

@Repository
public interface MarcUserRepository extends JpaRepository<UserEntity, Integer> {

}
