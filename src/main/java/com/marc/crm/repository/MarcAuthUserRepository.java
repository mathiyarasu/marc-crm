package com.marc.crm.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.marc.crm.entity.AuthUser;

@Repository
public interface MarcAuthUserRepository extends JpaRepository<AuthUser, String> {

	@Query("SELECT b FROM AuthUser b Where b.userid = ?1 And b.status = 'A' ")
	public Optional<AuthUser> findByUserid(String userid);

	@Query("SELECT b FROM AuthUser b Where b.userid = ?1 And b.status = 'N' ")
	public Optional<AuthUser> findUnAuthUser(String userid);
}
