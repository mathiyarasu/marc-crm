package com.marc.crm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.marc.crm.entity.CrmLeadInfo;

@Repository
public interface CrmLeadRepository extends JpaRepository<CrmLeadInfo, Integer> {

	@Query("SELECT b FROM CrmLeadInfo b ")
	public List<CrmLeadInfo> findAll();

	@Query("SELECT b FROM CrmLeadInfo b WHERE b.leadNo = :leadNo ")
	public CrmLeadInfo findbyNo(@Param("leadNo") String leadNo);

	@Modifying
	@Query("DELETE FROM CrmLeadInfo b WHERE b.leadNo = :leadNo ")
	public int deleteByLeadNo(@Param("leadNo") String leadNo);
}
