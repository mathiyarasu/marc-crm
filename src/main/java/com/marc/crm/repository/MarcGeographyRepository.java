package com.marc.crm.repository;

import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.marc.crm.entity.Geography;

@Repository
public interface MarcGeographyRepository extends JpaRepository<Geography, Long> {

	Optional<Geography> findById(Long parseInt);

	Set<Geography> findByType(String type);

	@Modifying
	@Query(value = "insert into geography (code,description,type,parent) values (:code,:desc,:type,:parent);", nativeQuery = true)
	@Transactional
	int insert(String code, String desc, String type, String parent);

}
