package com.marc.crm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.marc.crm.entity.UserDetails;

@Repository
public interface MarcUserDetailsRepository extends JpaRepository<UserDetails, String> {

	public void findBy();

	public UserDetails findByUserid(String userid);

}
