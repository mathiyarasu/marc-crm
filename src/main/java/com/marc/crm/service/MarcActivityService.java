package com.marc.crm.service;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marc.crm.dto.ResponseDto;
import com.marc.crm.dto.UserActivityDto;
import com.marc.crm.entity.LoginDetails;
import com.marc.crm.process.MarcActivityProcess;
import com.marc.crm.util.Assistant;
import com.marc.crm.util.MarcException;

@Service
@Transactional
public class MarcActivityService {

	@Autowired
	MarcActivityProcess activityProcess;
	ResponseDto responseDto = new ResponseDto();

	public String createActivityInfo(Map<?, ?> requestMap) {
		Integer key = 0;
		try {

			LoginDetails loginDet = Assistant.convertJSONtoEntity(requestMap, "marc_info", LoginDetails.class);
			// loginDet.setSessionid(hashKey);
			key = activityProcess.createActivityInfo(loginDet);

		} catch (Exception e) {
			throw new MarcException("MC-ACT-001", e.getMessage());
		}
		return key + "";
	}

	public ResponseDto getActivityInfo(Map<?, ?> requestMap) {
		boolean status = false;
		List<UserActivityDto> returnVal = null;
		responseDto = new ResponseDto();
		try {

			String userid = Assistant.convertJSONtoEntity(requestMap, "marc_userid", String.class);
			returnVal = activityProcess.getActivityInfo(userid);
			responseDto.setStatus(status);
			responseDto.setData(returnVal);

		} catch (Exception e) {
			throw new MarcException("MC-ACT-002", e.getMessage());
		}
		return responseDto;
	}

	public ResponseDto updateActivityInfo(Map<?, ?> requestMap) {
		boolean status = false;
		responseDto = new ResponseDto();
		try {

			String logout = Assistant.convertJSONtoEntity(requestMap, "marc_logout", String.class);
			String key = Assistant.convertJSONtoEntity(requestMap, "marc_key", String.class);
			activityProcess.updateActivityInfo(logout, key);
			responseDto.setStatus(status);

		} catch (Exception e) {
			throw new MarcException("MC-ACT-003", e.getMessage());
		}
		return responseDto;
	}

	public String getActivitySno(String key) {
		try {
			return activityProcess.getActivitySno(key);
		} catch (Exception e) {
			throw new MarcException("MC-ACT-003", e.getMessage());
		}
	}

}
