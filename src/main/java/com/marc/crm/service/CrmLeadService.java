package com.marc.crm.service;

import java.util.List;

import com.marc.crm.entity.CrmLeadInfo;

public interface CrmLeadService {

	List<CrmLeadInfo> getLead();

	CrmLeadInfo getLeadByNo(String leadNo);

	CrmLeadInfo addLead(CrmLeadInfo crmLeadInfo);

	Integer deleteByLeadNo(String leadNo);

	CrmLeadInfo updateLead(CrmLeadInfo crmLeadInfo);

}
