package com.marc.crm.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.marc.crm.entity.AuthUser;
import com.marc.crm.process.MarcAuthUserDetails;
import com.marc.crm.repository.MarcAuthUserRepository;

@Service
@Transactional
public class MarcUserDetailsService implements UserDetailsService {

	@Autowired
	private MarcAuthUserRepository authUserRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		Optional<AuthUser> authUser = authUserRepository.findByUserid(username);
		authUser.orElseThrow(() -> new UsernameNotFoundException("User Name Not Found ~" + username));
		return authUser.map(MarcAuthUserDetails::new).get();
	}

}
