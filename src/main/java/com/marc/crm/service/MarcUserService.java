package com.marc.crm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marc.crm.entity.UserEntity;
import com.marc.crm.process.MarcUserProcess;

@Service
public class MarcUserService {

	@Autowired
	MarcUserProcess marcUserProcess;

	public List<UserEntity> getUser() {
		return marcUserProcess.getUser();

	}

	public UserEntity addUser(UserEntity userEntity) {
		return marcUserProcess.addUser(userEntity);

	}

	public String deleteUser(Integer userId) {
		return marcUserProcess.deleteUser(userId);

	}

	public UserEntity updateUser(UserEntity userEntity) {
		return marcUserProcess.updateUser(userEntity);

	}
}
