package com.marc.crm.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marc.crm.entity.CrmLeadInfo;
import com.marc.crm.repository.CrmLeadRepository;

@Service
public class BaseCrmLeadService implements CrmLeadService {

	@Autowired
	CrmLeadRepository crmLeadRepository;
	
	@Override
	public List<CrmLeadInfo> getLead() {		
		return crmLeadRepository.findAll();
	}
	
	@Override
	public CrmLeadInfo getLeadByNo(String leadNo) { 
		return crmLeadRepository.findbyNo(leadNo);
	}

	@Transactional
	@Override
	public CrmLeadInfo addLead(CrmLeadInfo crmLeadInfo) {	
		return crmLeadRepository.save(crmLeadInfo);
	}

	@Transactional
	@Override
	public Integer deleteByLeadNo(String leadNo) {		
		return crmLeadRepository.deleteByLeadNo(leadNo);
	}

	@Transactional
	@Override
	public CrmLeadInfo updateLead(CrmLeadInfo crmLeadInfo) {
	    return crmLeadRepository.save(crmLeadInfo);
	}

}
