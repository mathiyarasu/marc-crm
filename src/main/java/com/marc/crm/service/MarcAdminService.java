package com.marc.crm.service;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.marc.crm.dto.ResponseDto;
import com.marc.crm.entity.Geography;
import com.marc.crm.process.MarcAdminProcess;
import com.marc.crm.util.Assistant;
import com.marc.crm.util.MarcException;
import com.marc.crm.util.SecurityHash;

@Service
@Transactional
public class MarcAdminService {

	@Autowired
	MarcAdminProcess adminProcess;

	@Autowired
	private SecurityHash securityHash;

	@Value("${enc_key}")
	private String enckey;

	@Autowired
	private Environment env;

	ResponseDto responseDto = new ResponseDto();

	public ResponseDto updateFirstlogin(Map<?, ?> requestMap) {
		responseDto = new ResponseDto();
		boolean status = false;

		try {

			String userid = Assistant.convertJSONtoEntity(requestMap, "marc_userid", String.class);
			String mode = Assistant.convertJSONtoEntity(requestMap, "marc_mode", String.class);

			status = adminProcess.updateFirstlogin(userid, mode);
			responseDto.setStatus(true);

		} catch (MarcException e) {
			throw e;
		} catch (Exception e) {
			throw new MarcException("MC-ADM-001", e.getMessage());
		}
		return responseDto;
	}

	public ResponseDto getGeography(@Valid Map<?, ?> requestMap) {
		responseDto = new ResponseDto();

		try {

			String id = Assistant.convertJSONtoEntity(requestMap, "id", String.class);
			String type = Assistant.convertJSONtoEntity(requestMap, "type", String.class);
			responseDto.setData(adminProcess.getGeography(id, type));
			responseDto.setStatus(true);

		} catch (Exception e) {
			throw new MarcException("MC-ADM-006", e.getMessage());
		}
		return responseDto;
	}

	public ResponseDto bulkInsertGeography(@Valid Map<?, ?> requestMap) {
		responseDto = new ResponseDto();

		try {

			List list = Assistant.convertJSONtoEntity(requestMap, "geographyList", List.class);
			List<Geography> geographyList = new ObjectMapper().convertValue(list, new TypeReference<List<Geography>>() {
			});

			responseDto.setData(adminProcess.bulkInsertGeography(geographyList));
			responseDto.setStatus(true);

		} catch (Exception e) {
			throw new MarcException("MC-ADM-006", e.getMessage());
		}
		return responseDto;
	}

	public ResponseDto saveGeography(@Valid Map<?, ?> requestMap) {
		responseDto = new ResponseDto();

		try {

			Geography geo = Assistant.convertJSONtoEntity(requestMap, "geography", Geography.class);

			responseDto.setData(adminProcess.saveGeography(geo));
			responseDto.setStatus(true);

		} catch (Exception e) {
			e.printStackTrace();
			throw new MarcException("MC-ADM-006", e.getMessage());
		}
		return responseDto;
	}

	public ResponseDto getKey() {
		responseDto = new ResponseDto();

		try {

			responseDto.setData(securityHash.encodeKey(enckey));
			responseDto.setStatus(true);

		} catch (Exception e) {
			throw new MarcException("MC-ADM-005", e.getMessage());
		}
		return responseDto;
	}
}
