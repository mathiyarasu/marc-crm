package com.marc.crm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.marc.crm.entity.UserEntity;
import com.marc.crm.service.MarcUserService;

@RestController
@RequestMapping("/user")
public class CrmUserController {

	@Autowired
	MarcUserService marcUserService;

	@GetMapping("/getuser")
	public List<UserEntity> getUser() {
		return marcUserService.getUser();
	}

	@PostMapping("/adduser")
	public UserEntity addUser(@RequestBody UserEntity userEntity) {
		return marcUserService.addUser(userEntity);

	}

	@DeleteMapping("/deleteuser/{userId}")
	public String deleteUser(@PathVariable("userId") Integer userId) {
		return marcUserService.deleteUser(userId);

	}

	@PutMapping("/updateuser")
	public UserEntity updateUser(@RequestBody UserEntity userEntity) {
		return marcUserService.updateUser(userEntity);

	}

}
