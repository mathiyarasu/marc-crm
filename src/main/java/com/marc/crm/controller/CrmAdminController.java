package com.marc.crm.controller;

import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.marc.crm.dto.AuthRequestDto;
import com.marc.crm.dto.AuthResponseDto;
import com.marc.crm.dto.ResponseDto;
import com.marc.crm.service.MarcActivityService;
import com.marc.crm.service.MarcAdminService;
import com.marc.crm.service.MarcUserDetailsService;
import com.marc.crm.util.Assistant;
import com.marc.crm.util.JWTUtility;
import com.marc.crm.util.MarcException;
import com.marc.crm.util.SecurityHash;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "/marc-admin", produces = { "application/json", "application/xml" }, consumes = {
		"application/json" }, method = RequestMethod.POST)
public class CrmAdminController {
	Logger log = LoggerFactory.getLogger(this.getClass());

	private MarcAdminService adminService;
	private AuthenticationManager authenticationManager;
	private MarcUserDetailsService userDetailsService;
	private JWTUtility jwtUtility;

	private MarcActivityService activityService;
	private CacheManager cacheManager;
	private SecurityHash securityHash;

	@Value("${enc_key}")
	private String enckey;

	@Autowired
	public CrmAdminController(MarcAdminService adminService, AuthenticationManager authenticationManager,
			MarcUserDetailsService userDetailsService, JWTUtility jwtUtility, MarcActivityService activityService,
			CacheManager cacheManager, SecurityHash securityHash) {
		this.adminService = adminService;
		this.authenticationManager = authenticationManager;
		this.userDetailsService = userDetailsService;
		this.jwtUtility = jwtUtility;
		this.activityService = activityService;
		this.cacheManager = cacheManager;
		this.securityHash = securityHash;
	}

	@PostMapping(value = "updatefirstlogin")
	public ResponseEntity<ResponseDto> updateFirstlogin(@Valid @RequestBody Map<?, ?> requestMap) {
		ResponseDto responseDto = new ResponseDto();
		try {
			responseDto = adminService.updateFirstlogin(requestMap);
			log.info("User Updated ");
		} catch (MarcException ex) {
			Assistant.addErrorResponse("updateuser", ex, responseDto, log);
		}
		clearAllCaches(requestMap);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}

	@PostMapping(value = "loginuser")
	public ResponseEntity<AuthResponseDto> loginuser(@RequestBody @Valid AuthRequestDto authDto) throws Exception {
		AuthResponseDto responseDto = new AuthResponseDto();
		try {

			log.info("Authenticating user " + authDto.getUsername() + "!!" + Thread.currentThread().getId());
			log.info("Authenticating pwd ######## " + authDto.getPassword());

			authDto.setPassword(securityHash.decrypt(authDto.getPassword(), securityHash.encodeKey(enckey)));
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(authDto.getUsername(), authDto.getPassword()));
			log.info("End Authenticating user " + authDto.getUsername());

		} catch (BadCredentialsException ex) {
			log.debug("BAD BNI CREDENTIAL EXCEPTION" + ex.getMessage());
			throw new Exception("BAD BNI CREDENTIAL EXCEPTION", ex);

		}

		UserDetails userDet = userDetailsService.loadUserByUsername(authDto.getUsername());

		responseDto.setMarcTokan(jwtUtility.generateToken(userDet));
		log.info("Token generated for user " + authDto.getUsername());
		if (userDet.getAuthorities() != null)
			responseDto.setRole(userDet.getAuthorities().toString());
		responseDto.setUsername(userDet.getUsername());
		responseDto.setActsno(activityService.createActivityInfo(authDto.getActInfo()));
		clearAllCaches(null);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}

	@PostMapping(value = "getkey")
	public ResponseEntity<ResponseDto> getKey(@Valid @RequestBody Map<?, ?> requestMap) throws Exception {
		ResponseDto responseDto = new ResponseDto();
		try {

			log.debug("Key Generation strated..");
			responseDto = adminService.getKey();
			log.debug("Key Generated..");

		} catch (MarcException ex) {
			Assistant.addErrorResponse("getuserdetail", ex, responseDto, log);
		}
		clearAllCaches(requestMap);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}

	@PostMapping(value = "getgeography")
	public ResponseEntity<ResponseDto> getGeography(@Valid @RequestBody Map<?, ?> requestMap) {
		ResponseDto responseDto = new ResponseDto();
		try {
			responseDto = adminService.getGeography(requestMap);

		} catch (MarcException ex) {
			Assistant.addErrorResponse("getGeography", ex, responseDto, log);
		} catch (Exception ex) {
			responseDto = Assistant.addErrorResponse("getGeography", new MarcException("MC-ADM-001", ex.getMessage()),
					responseDto, log);
		}
		clearAllCaches(requestMap);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}

	@PostMapping(value = "bulkinsertgeography")
	public ResponseEntity<ResponseDto> bulkInsertGeography(@Valid @RequestBody Map<?, ?> requestMap) {
		ResponseDto responseDto = new ResponseDto();
		try {
			responseDto = adminService.bulkInsertGeography(requestMap);

		} catch (MarcException ex) {
			Assistant.addErrorResponse("bulkInsertGeography", ex, responseDto, log);
		} catch (Exception ex) {
			responseDto = Assistant.addErrorResponse("saveGeography", new MarcException("MC-ADM-001", ex.getMessage()),
					responseDto, log);
		}
		clearAllCaches(requestMap);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}

	@PostMapping(value = "savegeography")
	public ResponseEntity<ResponseDto> saveGeography(@Valid @RequestBody Map<?, ?> requestMap) {
		ResponseDto responseDto = new ResponseDto();
		try {
			responseDto = adminService.saveGeography(requestMap);

		} catch (MarcException ex) {
			Assistant.addErrorResponse("saveGeography", ex, responseDto, log);
		} catch (Exception ex) {
			responseDto = Assistant.addErrorResponse("saveGeography", new MarcException("MC-ADM-001", ex.getMessage()),
					responseDto, log);
		}
		clearAllCaches(requestMap);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}

	public void clearAllCaches(Map<?, ?> requestMap) {
		if (requestMap != null)
			requestMap.clear();
		cacheManager.getCacheNames().stream().forEach(cacheName -> cacheManager.getCache(cacheName).clear());
	}
}
