package com.marc.crm.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.marc.crm.dto.ResponseModel;
import com.marc.crm.entity.CrmLeadInfo;
import com.marc.crm.service.CrmLeadService;
import com.marc.crm.util.ErrorCodes;

@RestController
@RequestMapping("/web/v1/lead")
@CrossOrigin(origins="http://localhost:4200")
public class CrmLeadController {

	@Autowired
	CrmLeadService crmLeadService;

	Logger log = LoggerFactory.getLogger(this.getClass());

	@GetMapping("/getlead")
	public ResponseModel getLead() {
		ResponseModel res = new ResponseModel();
		List<CrmLeadInfo> crmLeadInfoList = null;

		try {
			crmLeadInfoList = crmLeadService.getLead();
			res.setMessage("List of Leads");
			res.setStatus(ErrorCodes.SUCCESS);
			res.setPayload(crmLeadInfoList);
		} catch (Exception e) {
			log.error("Exception in CrmLeadController :: " + e.getMessage());
			res.setMessage("Issue in getLead : " + e.getMessage());
			res.setStatus(ErrorCodes.FAILURE);
			res.setPayload(crmLeadInfoList);
		}
		return res;
	}

	@GetMapping("/getlead/{leadNo}")
	public ResponseModel getLeadByNo(@PathVariable("leadNo") String leadNo) {
		ResponseModel res = new ResponseModel();
		CrmLeadInfo crmLeadInfo = null;
		try {
			crmLeadInfo = crmLeadService.getLeadByNo(leadNo);
			res.setMessage("Get Leads by No:" + leadNo);
			res.setStatus(ErrorCodes.SUCCESS);
			res.setPayload(crmLeadInfo);
		} catch (Exception e) {
			log.error("Exception in CrmLeadController : getLeadByNo : " + e.getMessage());
			res.setMessage("Issue in getLeadByNo : " + e.getMessage());
			res.setStatus(ErrorCodes.FAILURE);
			res.setPayload(crmLeadInfo);
		}
		return res;
	}


	@PostMapping("/addlead")
	public ResponseModel addLead(@RequestBody CrmLeadInfo crmLeadInfo) {
		ResponseModel res = new ResponseModel();
		CrmLeadInfo crmLeadInfoRes = null;
		try {
			crmLeadInfoRes = crmLeadService.addLead(crmLeadInfo);
			res.setMessage("Record Created Successfully");
			res.setStatus(ErrorCodes.SUCCESS);
			res.setPayload(crmLeadInfoRes);
		} catch (Exception e) {
			log.error("Exception in CrmLeadController : addLead : " + e.getMessage());
			res.setMessage("Issue in addLead : " + e.getMessage());
			res.setStatus(ErrorCodes.FAILURE);
			res.setPayload(crmLeadInfoRes);
		}

		return res;
	}

	@PutMapping("/updatelead/{leadNo}")
	public ResponseModel updateLead(@RequestBody CrmLeadInfo crmLeadInfo, @PathVariable("leadNo") String leadNo) {
		ResponseModel res = new ResponseModel();
		CrmLeadInfo crmLeadInfoRes = null;
		try {
			crmLeadService.updateLead(crmLeadInfo);
			res.setMessage("Record Updated Successfully");
			res.setStatus(ErrorCodes.SUCCESS);
			res.setPayload(crmLeadInfoRes);
		} catch (Exception e) {
			log.error("Exception in CrmLeadController : updateLead : " + e.getMessage());
			res.setMessage("Issue in updateLead : " + e.getMessage());
			res.setStatus(ErrorCodes.FAILURE);
			res.setPayload(crmLeadInfo);
		}

		return res;

	}

	@DeleteMapping("/deletelead/{leadNo}")
	public ResponseModel deleteLead(@PathVariable("leadNo") String leadNo) {
		ResponseModel res = new ResponseModel();
		Integer crmLeadInfo = null;
		try {
			crmLeadInfo = crmLeadService.deleteByLeadNo(leadNo);
			res.setMessage("Record Deleted Successfully");
			res.setStatus(ErrorCodes.SUCCESS);
			res.setPayload(crmLeadInfo);
		} catch (Exception e) {
			log.error("Exception in CrmLeadController : deleteLead : " + e.getMessage());
			res.setMessage("Issue in deleteLead : " + e.getMessage());
			res.setStatus(ErrorCodes.FAILURE);
			res.setPayload(crmLeadInfo);
		}

		return res;
	}

}
